#!/usr/bin/python3


"""
Calculates the efficiency of Reversible Rankine Cycle
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '25/11/2019'
__status__  = 'Development'


import os
import json
from chart_plotting import ChartPlotting


def read_json(filename):
    folder = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(folder, "json", filename)) as f:
        return json.load(f)


class Entropy:
    def s1l(self):
        return self.vap_prop[str(self.p1)]["entropy_l"]

    def s1v(self):
        return self.vap_prop[str(self.p1)]["entropy"][0]

    def s1(self):
        ix_t1 = self.calc_index_t1()
        return self.vap_prop[str(self.p1)]["entropy"][ix_t1]

    def s2l(self):
        return self.two_phase_prop[str(self.p2)]["entropy_l"]

    def s2(self):
        return self.s1()


class Enthalpy:
    def h1(self):
        ix_t1 = self.calc_index_t1()
        return self.vap_prop[str(self.p1)]["enthalpy"][ix_t1]

    def h2l(self):
        return self.two_phase_prop[str(self.p2)]["enthalpy_l"]


class RankineCycle(Entropy, Enthalpy, ChartPlotting):
    def __init__(self):
        self.p1 = 16.0
        self.t1 = 500
        self.p2 = 0.004
        ChartPlotting.__init__(self)

    def read_properties(self):
        self.sat_prop = read_json("saturation_properties.json")
        self.vap_prop = read_json("vapor_properties.json")
        self.two_phase_prop = read_json("two_phase_properties.json")

    def select_ts_curve_data(self):
        # data to draw T-s curve
        self.temp = self.sat_prop["temperature"]
        self.sl = self.sat_prop["entropy_l"]
        self.sv = self.sat_prop["entropy_v"]

    def calc_index_t1(self):
        return self.vap_prop[str(self.p1)]["temperature"].index(self.t1)

    def calc_condenser_temperature(self):
        t2sat = self.two_phase_prop[str(self.p2)]["sat_temp"]
        return t2sat

    def calc_cycle_points(self):
        # cycle curve data
        ix_t1 = self.calc_index_t1()
        supt = list(self.vap_prop[str(self.p1)]["temperature"][:ix_t1+1])
        sups = list(self.vap_prop[str(self.p1)]["entropy"][:ix_t1+1])

        i = 0
        ii = 0
        t = self.sat_prop["temperature"][ii]
        t1sat = self.vap_prop[str(self.p1)]["sat_temp"]
        t2sat = self.calc_condenser_temperature()
        while t < t1sat:
            if t > t2sat and i == 0:
                i = ii
            ii += 1
            t = self.sat_prop["temperature"][ii]

        liqt = list(self.sat_prop["temperature"][i:ii])
        liqs = list(self.sat_prop["entropy_l"][i:ii])

        s2l = self.s2l()
        s1l = self.s1l()
        s1v = self.s1v()
        s1 = self.s1()
        s2 = self.s2()

        cycle_t = [t2sat] + liqt + [t1sat, t1sat] + supt + [self.t1, t2sat, t2sat]
        cycle_s = [s2l] + liqs + [s1l, s1v] + sups + [s1, s2, s2l]

        self.pos_1x = s1
        self.pos_1y = self.t1
        self.pos_2x = s1
        self.pos_2y = t2sat

        return cycle_s, cycle_t

    def calc_efficiency(self):
        # average t1 and efficiency
        h1 = self.h1()
        h2l = self.h2l()
        s2l = self.s2l()
        s1 = self.s1()
        t1avg = (h1 - h2l) / (s1 - s2l) # in Kelvin!
        t2sat = self.calc_condenser_temperature()
        eff = 1 - (t2sat+273.15) / t1avg
        return eff

    def run_calc(self):
        self.read_properties()
        self.select_ts_curve_data()
        self.cycle_s, self.cycle_t = self.calc_cycle_points()
        self.eff = self.calc_efficiency()
        return self.cycle_s, self.cycle_t, self.eff


def run_app():
    rcobj = RankineCycle()
    rcobj.run_calc()
    rcobj.run_plot()


if __name__ == "__main__":
    run_app()
