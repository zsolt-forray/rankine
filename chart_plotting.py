#!/usr/bin/python3


"""
Chart plotting
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '24/11/2019'
__status__  = 'Development'


from matplotlib import pyplot as plt
from chart_settings import ChartSettings


class ChartPlotting(ChartSettings):
    def __init__(self):
        ChartSettings.__init__(self)

    def run_plot(self):
        self.set_chart()
        self.ax.set_ylim(0, 600)
        self.ax.set_xlim(0, 10)
        self.ax.plot(self.sl, self.temp, color="k", linewidth=0.7)
        self.ax.plot(self.sv, self.temp, color="k", linewidth=0.7)
        self.l, = self.ax.plot(self.cycle_s, self.cycle_t, color="r", linewidth=1.5)

        self.button_inc_p1.on_clicked(self.inc_p1)
        self.button_dec_p1.on_clicked(self.dec_p1)

        self.button_inc_t1.on_clicked(self.inc_t1)
        self.button_dec_t1.on_clicked(self.dec_t1)

        self.button_inc_p2.on_clicked(self.inc_p2)
        self.button_dec_p2.on_clicked(self.dec_p2)

        self.button_close.on_clicked(ChartPlotting.close_chart)

        self.pos_1 = self.ax.annotate(" p1, t1", (self.cycle_s[49], self.cycle_t[49]))
        self.pos_2 = self.ax.annotate(" p2", (self.cycle_s[50], self.cycle_t[50]))

        plt.show()

    def inc_p1(self, evt):
        if self.p1 < 18:
            self.p1 += 0.5
        cycle_s, cycle_t, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(e))
        self.p1_text.set_text("{}MPa".format(self.p1))
        self.l.set_ydata(cycle_t)
        self.l.set_xdata(cycle_s)
        self.pos_1.set_position((self.pos_1x, self.pos_1y))
        self.pos_2.set_position((self.pos_2x, self.pos_2y))
        self.fig.canvas.draw_idle()

    def dec_p1(self, evt):
        if self.p1 > 16:
            self.p1 -= 0.5
        cycle_s, cycle_t, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(e))
        self.p1_text.set_text("{}MPa".format(self.p1))
        self.l.set_ydata(cycle_t)
        self.l.set_xdata(cycle_s)
        self.pos_1.set_position((self.pos_1x, self.pos_1y))
        self.pos_2.set_position((self.pos_2x, self.pos_2y))
        self.fig.canvas.draw_idle()

    def inc_t1(self, evt):
        if self.t1 < 570:
            self.t1 += 10
        cycle_s, cycle_t, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(e))
        self.t1_text.set_text("{}°C".format(self.t1))
        self.l.set_ydata(cycle_t)
        self.l.set_xdata(cycle_s)
        self.pos_1.set_position((self.pos_1x, self.pos_1y))
        self.pos_2.set_position((self.pos_2x, self.pos_2y))
        self.fig.canvas.draw_idle()

    def dec_t1(self, evt):
        if self.t1 > 500:
            self.t1 -= 10
        cycle_s, cycle_t, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(e))
        self.t1_text.set_text("{}°C".format(self.t1))
        self.l.set_ydata(cycle_t)
        self.l.set_xdata(cycle_s)
        self.pos_1.set_position((self.pos_1x, self.pos_1y))
        self.pos_2.set_position((self.pos_2x, self.pos_2y))
        self.fig.canvas.draw_idle()

    def inc_p2(self, evt):
        if self.p2 < 0.007:
            self.p2 += 0.001
        cycle_s, cycle_t, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(e))
        self.p2_text.set_text("{}MPa".format(self.p2))
        self.l.set_ydata(cycle_t)
        self.l.set_xdata(cycle_s)
        self.pos_1.set_position((self.pos_1x, self.pos_1y))
        self.pos_2.set_position((self.pos_2x, self.pos_2y))
        self.fig.canvas.draw_idle()

    def dec_p2(self, evt):
        if self.p2 > 0.004:
            self.p2 -= 0.001
        cycle_s, cycle_t, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(e))
        self.p2_text.set_text("{}MPa".format(self.p2))
        self.l.set_ydata(cycle_t)
        self.l.set_xdata(cycle_s)
        self.pos_1.set_position((self.pos_1x, self.pos_1y))
        self.pos_2.set_position((self.pos_2x, self.pos_2y))
        self.fig.canvas.draw_idle()

    @staticmethod
    def close_chart(evt):
        plt.close()
