# Reversible Rankine Cycle

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/d9dd75d5e428447a915d3aebdd3f2af3)](https://www.codacy.com/manual/forray.zsolt/rankine?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=Zsolt-Forray/rankine&amp;utm_campaign=Badge_Grade)
[![Python 3.7](https://img.shields.io/badge/python-3.7-blue.svg)](https://www.python.org/downloads/release/python-370/)

## Description
This tool calculates the efficiency of Reversible Rankine Cycle.

## Usage

![Screenshot](/png/fig.png)

### Usage Example

```python
#!/usr/bin/python3

import rankine_cycle as rc

rc.run_app()
```

**Parameters:**

* p1: Isobar Heat Transfer (MPa)
* t1: Steam Turbine Entry Temperature (°C)
* p2: Isobar Heat Rejection (MPa)

## Contributions
Contributions to this repository are always welcome.
This repo is maintained by Zsolt Forray (forray.zsolt@gmail.com).
